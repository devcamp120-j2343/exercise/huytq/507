class Company {
    id;
    companyName;
    contact;
    country;
    
    constructor(paramId, paramCompanyName, paramContact, paramCountry) {
        this.id = paramId;
        this.companyName = paramCompanyName;
        this.contact = paramContact;
        this.country = paramCountry;
    }
}

module.exports = Company;