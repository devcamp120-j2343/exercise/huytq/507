const express = require("express");
// Khởi tạo Express App
const app = express();
const port = 8000;
const companyRouter = require("./app/routes/company.router");
app.use("/api/v1/companies", companyRouter);
app.listen(port, () => {
    console.log(`App Listening on port ${port}`);
})
