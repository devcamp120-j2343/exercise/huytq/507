const express = require("express");

const Company = require('../../company');

const companyMiddleware = require("../middlewares/company.middlewares");

const router = express.Router();

router.use((req, res, next) => {
    console.log("Request method: ", req.method);

    next();
});

router.get("/", companyMiddleware.getAllCompanyMiddleware, (req, res) => {
    var AllCompany = [];
    var company1 = new Company(1,"Alfreds Futterkiste","Maria Anders","Germany");
    AllCompany.push(company1);
    var company2 = new Company(2,"Centro comercial Moctezuma","Francisco Chang","Mexico");
    AllCompany.push(company2);
    var company3 = new Company(3,"Ernst Handel","Roland Mendel","Austria");
    AllCompany.push(company3);
    var company4 = new Company(4,"Island Trading","Helen Bennett","UK");
    AllCompany.push(company4);
    var company5 = new Company(5,"Laughing Bacchus Winecellars","Yoshi Tannamuri","Canada");
    AllCompany.push(company5);
    var company6 = new Company(6,"Magazzini Alimentari Riuniti","Giovanni Rovelli","Italy");
    AllCompany.push(company6);
    res.json(AllCompany);
})

router.post("/", companyMiddleware.createCompanyMiddleware, (req, res) => {
    res.json({
        message: "Create company"
    })
});

router.get("/:companyId", companyMiddleware.getDetailCompanyMiddleware, (req, res) => {
    var companyId = req.params.companyId;

    res.json({
        message: "Get company id = " + companyId
    })
});

router.put("/:companyId", companyMiddleware.updateCompanyMiddleware, (req, res) => {
    var companyId = req.params.companyId;

    res.json({
        message: "Update company id = " + companyId
    })
});

router.delete("/:companyId", companyMiddleware.deleteCompanyMiddleware, (req, res) => {
    var companyId = req.params.companyId;

    res.json({
        message: "Delete company id = " + companyId
    })
});

module.exports = router;