const getAllCompanyMiddleware = (req, res, next) => {
    console.log("Get all company Middleware");

    next();
}

const createCompanyMiddleware = (req, res, next) => {
    console.log("Create company Middleware");

    next();
}

const getDetailCompanyMiddleware = (req, res, next) => {
    console.log("Get detail company Middleware");

    next();
}

const updateCompanyMiddleware = (req, res, next) => {
    console.log("Update company Middleware");

    next();
}

const deleteCompanyMiddleware = (req, res, next) => {
    console.log("Delete company Middleware");

    next();
}

module.exports = {
   getAllCompanyMiddleware,
   createCompanyMiddleware,
   getDetailCompanyMiddleware,
   updateCompanyMiddleware,
   deleteCompanyMiddleware,
}